grafana_password                  = "foo"
kube_prometheus_additional_values = []
kube_prometheus_stack_version     = "58.6.1"
load_dashboards                   = []
pagerduty_service_key             = "noop"
redis_address                     = "default"
redis_password                    = "default"

slack_url = "https://localhost:8024"
