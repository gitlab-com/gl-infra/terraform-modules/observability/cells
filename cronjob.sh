#!/usr/bin/env bash

NAMESPACE=$(cat /var/run/secrets/kubernetes.io/serviceaccount/namespace)
RUNBOOKS_REPO_LOCAL=/mnt/runbooks/repo
RUNBOOKS_REPO_REMOTE=https://gitlab.com/gitlab-com/runbooks.git

if [ ! -d $RUNBOOKS_REPO_LOCAL ]; then
  git clone $RUNBOOKS_REPO_REMOTE $RUNBOOKS_REPO_LOCAL
else
  (cd $RUNBOOKS_REPO_LOCAL && git pull origin master)
fi

cd $RUNBOOKS_REPO_LOCAL

jb install
WD=$(mktemp -d)

# Generate baseline metrics configuration for reference architecture
$RUNBOOKS_REPO_LOCAL/scripts/generate-reference-architecture-config.sh $RUNBOOKS_REPO_LOCAL/reference-architectures/get-hybrid/src/ $WD/metrics-catalog/config

# TODO POC: services rules fail to apply because of size:
#  The PrometheusRule "instrumentor-service-rules" is invalid: metadata.annotations: Too long: must have at most 262144 bytes

mkdir -p $WD/manifests
for ruleFile in $WD/metrics-catalog/config/prometheus-rules/*.yml; do
  fileName=$(basename $ruleFile .yml)
  cat <<EOF >$WD/manifests/$fileName
---
apiVersion: monitoring.coreos.com/v1
kind: PrometheusRule
metadata:
  name: instrumentor-$fileName
  namespace: $NAMESPACE
  labels:
    release: kube-prometheus-stack
spec:
EOF
  # Add 6 spaces of indent to each line.
  cat $ruleFile | sed 's/^/  /g' >>$WD/manifests/$fileName

  head -n 20 $WD/manifests/$fileName

  kubectl -n $NAMESPACE apply -f $WD/manifests/$fileName
done

# Clean up after ourselves!
rm -rf $WD
