

resource "kubernetes_service_account_v1" "prom_rules_generator" {
  metadata {
    name      = "prom-rules-generator"
    namespace = local.monitoring_namespace
  }

  depends_on = [
    helm_release.kube_prometheus_stack # To create the `monitoring` namespace
  ]
}

resource "kubernetes_role_v1" "prom_rules_generator" {
  metadata {
    name      = "prom-rules-generator"
    namespace = local.monitoring_namespace
  }

  rule {
    api_groups = ["", "monitoring.coreos.com"]
    resources  = ["prometheusrules", "pods"]
    verbs      = ["get", "update", "create"]
  }

  depends_on = [
    helm_release.kube_prometheus_stack # To create the `monitoring` namespace
  ]
}

resource "kubernetes_role_binding_v1" "prom_rules_generator" {
  metadata {
    name      = "prom-rules-generator"
    namespace = local.monitoring_namespace
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = "prom-rules-generator"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "prom-rules-generator"
    namespace = local.monitoring_namespace
  }

  depends_on = [
    helm_release.kube_prometheus_stack # To create the `monitoring` namespace
  ]
}

resource "kubernetes_cron_job_v1" "prom_rules" {
  metadata {
    name      = "prom-rules-cronjob"
    namespace = local.monitoring_namespace
  }
  spec {
    concurrency_policy            = "Replace"
    failed_jobs_history_limit     = 5
    schedule                      = "* * * * *"
    timezone                      = "Etc/UTC"
    starting_deadline_seconds     = 10
    successful_jobs_history_limit = 10
    job_template {
      metadata {
        namespace = local.monitoring_namespace
      }
      spec {
        backoff_limit              = 2
        ttl_seconds_after_finished = 300
        template {
          metadata {
          }
          spec {
            service_account_name = "prom-rules-generator"
            container {
              name    = "prom-rule-generator"
              image   = "registry.gitlab.com/gitlab-com/runbooks:master"
              command = ["/bin/sh", "-c", "/scripts/cronjob.sh"]

              volume_mount {
                mount_path = "/mnt/runbooks"
                name       = "runbooks"
              }
              volume_mount {
                mount_path = "/scripts"
                name       = "scripts"
              }
              env {
                name  = "GIT_SSH_COMMAND"
                value = "ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"
              }

            }
            volume {
              name = "scripts"
              config_map {
                name         = "prom-rules-script"
                default_mode = "0777"
              }
            }
            volume {
              name = "runbooks"
              persistent_volume_claim {
                claim_name = "cronjob-runbooks"
              }
            }
          }
        }
      }
    }
  }

  depends_on = [
    helm_release.kube_prometheus_stack # To create the `monitoring` namespace
  ]
}

resource "kubernetes_config_map" "cronjob_script" {
  metadata {
    name      = "prom-rules-script"
    namespace = local.monitoring_namespace
  }

  data = {
    "cronjob.sh" = file("${path.module}/cronjob.sh")
  }

  depends_on = [
    helm_release.kube_prometheus_stack # To create the `monitoring` namespace
  ]
}

# persistent volume claim for runbooks
resource "kubernetes_persistent_volume_claim" "cronjob_runbooks" {
  metadata {
    name      = "cronjob-runbooks"
    namespace = local.monitoring_namespace
  }
  spec {
    access_modes = ["ReadWriteOnce"]
    resources {
      requests = {
        storage = "5Gi"
      }
    }
  }
  wait_until_bound = false

  depends_on = [
    helm_release.kube_prometheus_stack # To create the `monitoring` namespace
  ]
}
