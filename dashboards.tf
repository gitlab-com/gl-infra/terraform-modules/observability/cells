resource "kubernetes_config_map" "dashboards" {
  for_each = setunion(
    flatten([
      for v in var.load_dashboards : v.files
    ])
  )

  metadata {
    name      = "dashboard-${replace(replace(basename(lower(each.value)), "\\.json$", ""), "[._-]", "")}"
    namespace = local.monitoring_namespace
    labels = {
      grafana_dashboard = "1"
    }
  }

  data = {
    # Note, the dashboards are not sensitive, but we're using this as a mechanism to reduce
    # diff-cruft
    (basename(each.value)) = sensitive(file(each.value))
  }

  depends_on = [
    helm_release.kube_prometheus_stack,
  ]

  lifecycle {
    # Extra checks to ensure that upload all the expected dashboards...
    precondition {
      condition     = alltrue([for v in var.load_dashboards : (length(v.files) > 0 || !v.fail_on_empty)])
      error_message = "One or more of the sets of dashboards is missing. Configuration error?"
    }
  }
}
