---
include:
  - template: Jobs/SAST-IaC.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml
  - local: .gitlab-ci-asdf-versions.yml

# TODO: Not including template for now, because we need a different tf version
# https://gitlab.com/gitlab-com/gl-infra/pipeline-templates/-/blob/main/terraform-modules.gitlab-ci.yml?ref_type=heads

stages:
  - init
  - lint
  - validate
  - test
  - docs
  - release

.vault:
  id_tokens:
    VAULT_ID_TOKEN:
      aud: https://vault.gitlab.net

.terraform_changes:
  - "**/*.json"
  - "**/*.md"
  - "**/*.tf"
  - "**/*.yaml"

.terraform:
  image:
    name: hashicorp/terraform:${GL_ASDF_TERRAFORM_VERSION}
    entrypoint: ['']
  variables:
    TF_INPUT: "false"
    TF_IN_AUTOMATION: "true"
    TF_PLUGIN_CACHE_DIR: ${CI_PROJECT_DIR}/${TF_PLUGIN_CACHE_DIR_RELPATH}
    TF_PLUGIN_CACHE_DIR_RELPATH: .terraform.d/plugin-cache
  artifacts:
    expire_in: 3 hours
  before_script:
    - mkdir -p "${TF_PLUGIN_CACHE_DIR}"
  cache:
    key: terraform-plugins-cache
    paths:
      - ${TF_PLUGIN_CACHE_DIR_RELPATH}
    when: always
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes: !reference [.terraform_changes]

terraform-init:
  extends: .terraform
  stage: init
  needs: []
  artifacts:
    name: terraform-init-artifacts
    paths:
      - .terraform/
      - .terraform.lock.hcl
  before_script:
    - !reference [.terraform, before_script]
    - |
      echo "Add registry credentials..."
      cat > ~/.terraformrc <<EOF
      credentials "${CI_SERVER_HOST}" {
        token = "${CI_JOB_TOKEN}"
      }
      EOF
  script:
    - terraform init -backend=false

terraform-fmt:
  extends: .terraform
  stage: lint
  needs: []
  script:
    - terraform fmt -check -diff -recursive

tflint:
  extends: .terraform
  stage: lint
  needs:
    - job: terraform-init
      artifacts: true
  artifacts:
    reports:
      junit: ${TFLINT_OUTPUT_FILE}
  image:
    name: ghcr.io/terraform-linters/tflint:v${GL_ASDF_TFLINT_VERSION}
    entrypoint: ['']
  script:
    - tflint --call-module-type=all --force
    - tflint --call-module-type=all --format=junit > ${TFLINT_OUTPUT_FILE}
  variables:
    TFLINT_OUTPUT_FILE: tflint.junit

terraform-validate:
  extends: .terraform
  stage: validate
  needs:
    - job: terraform-init
      artifacts: true
  artifacts:
    reports:
      codequality: ${TF_VALIDATE_REPORT_FILE}
  before_script:
    - !reference [terraform-init, before_script]
    - apk add -U jq
    - |
      if [ -n "${TF_VALIDATE_DIR}" ]; then
        terraform -chdir="${TF_VALIDATE_DIR}" init -backend=false
      fi
  script:
    - terraform -chdir="${TF_VALIDATE_DIR}" validate || true
    - |
      terraform -chdir="${TF_VALIDATE_DIR}" validate -json \
        | jq -cr '.diagnostics[]' | while read -r diag; do
            checksum="$(echo "${diag}" | sha256sum | cut -d ' ' -f1)"
            echo "${diag}" | jq -r \
              --arg fingerprint "${checksum}" \
              '{
                 "description": "\(.summary): \(.detail)",
                 "fingerprint": $fingerprint,
                 "severity": (
                     if .severity == "error" then "critical"
                     elif .severity == "warning" then "major"
                     else .severity
                     end),
                 "location": {
                   "path": .range.filename,
                   "lines": {
                     "begin": .range.start.line | tonumber
                   }
                 }
               }'
          done \
        | jq --slurp > "${TF_VALIDATE_REPORT_FILE}"
  variables:
    TF_VALIDATE_REPORT_FILE: terraform-validate.report.json
    TF_VALIDATE_DIR: .

tfsec:
  extends: .terraform
  stage: test
  needs:
    - job: terraform-init
      artifacts: true
  allow_failure: true
  artifacts:
    reports:
      junit: ${TFSEC_OUTPUT_FILE}
  image:
    name: aquasec/tfsec-ci:v1.28.6
  script:
    # FIXME: --ignore-hcl-errors until this issue is resolved:
    # https://github.com/aquasecurity/tfsec/issues/2070
    - tfsec --ignore-hcl-errors --format lovely,junit --out "${TFSEC_OUTPUT}"
  variables:
    TFSEC_OUTPUT: tfsec
    TFSEC_OUTPUT_FILE: ${TFSEC_OUTPUT}.junit

checkov:
  extends: .terraform
  stage: test
  needs:
    - job: terraform-init
      artifacts: true
  allow_failure: true
  artifacts:
    reports:
      sast: ${TF_CHECKOV_FILE}
  image:
    name: bridgecrew/checkov:${GL_ASDF_CHECKOV_VERSION}
    entrypoint: ['']
  script:
    - checkov --directory . --compact --output cli -o gitlab_sast --output-file-path console,"${TF_CHECKOV_FILE}" || true
  variables:
    TF_CHECKOV_FILE: terraform.checkov.json

kics-iac-sast:
  needs: []
  rules:
    - if: $SAST_DISABLED == 'true' || $SAST_DISABLED == '1'
      when: never
    - if: $SAST_EXCLUDED_ANALYZERS =~ /kics/
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes: !reference [.terraform_changes]

publish-terraform-docs:
  extends: .vault
  stage: docs
  image:
    name: quay.io/terraform-docs/terraform-docs:${GL_ASDF_TERRAFORM_DOCS_VERSION}
    entrypoint: ['']
  secrets:
    TERRAFORM_DOCS_TOKEN:
      file: false
      vault: access_tokens/${VAULT_AUTH_PATH}/gitlab-com/gl-infra/terraform-modules/_group_access_tokens/terraform-docs/token@ci
  variables:
    CLONE_DIR: ci_tf_docs
  before_script:
    - apk add -U git
    - echo "Cloning ${CI_PROJECT_URL} into ${CLONE_DIR}"
    - git config --global user.email "${GITLAB_USER_EMAIL}"
    - git config --global user.name "${GITLAB_USER_NAME}"
    - git clone ${CI_SERVER_PROTOCOL}://gitlab-ci-token:${TERRAFORM_DOCS_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git ${CLONE_DIR}
  script:
    - cd ${CLONE_DIR}
    - terraform-docs .
    - git add .
    - 'git commit -m "chore(docs): update README.md - auto generated terraform-docs." || (echo "No changes to add" && exit 0)'
    - git push ${CI_SERVER_PROTOCOL}://gitlab-ci-token:${TERRAFORM_DOCS_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git ${CI_DEFAULT_BRANCH}
  rules:
    - if: $CANONICAL && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes: !reference [.terraform_changes]
      exists:
        - .terraform-docs.yml

publish:
  extends: .vault
  stage: release
  image: node:lts-alpine3.17
  secrets:
    GITLAB_TOKEN:
      file: false
      vault: access_tokens/${VAULT_AUTH_PATH}/gitlab-com/gl-infra/terraform-modules/_group_access_tokens/semantic-release/token@ci
  before_script:
    - apk add -U git
    - npm install -g semantic-release@^21 @semantic-release/gitlab@^12
  script:
    - semantic-release
  rules:
    - if: $CANONICAL && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

publish-registry:
  image: curlimages/curl:latest
  stage: release
  variables:
    TERRAFORM_MODULE_DIR: "."  # Module path
    TERRAFORM_MODULE_NAME: ${CI_PROJECT_NAME}  # Module name
    TERRAFORM_MODULE_REGEX: "^[a-z]([a-z0-9-]){1,63}$"  # Module name must match for registry upload.
    TERRAFORM_MODULE_SYSTEM: ""  # Module system or provider (e.g. aws, google)
    TERRAFORM_MODULE_VERSION: ${CI_COMMIT_TAG}  # Module version
  script:
    # Validate module name - https://docs.gitlab.com/ee/user/packages/terraform_module_registry/#publish-a-terraform-module
    - if [[ ! "${TERRAFORM_MODULE_NAME}" =~ "${TERRAFORM_MODULE_REGEX}" ]]; then
        printf "\nIllegal Module Name!\nMust match ${TERRAFORM_MODULE_REGEX}\nYou can overwrite the module name via the TERRAFORM_MODULE_NAME CI variable (Default is CI_PROJECT_NAME)\n\n";
        exit 2; fi
    # Sanitize version, remove `^v`
    - TERRAFORM_MODULE_VERSION=${TERRAFORM_MODULE_VERSION#v}
    # Create archive, exclude hidden files/folders
    - tar -cvzf "${TERRAFORM_MODULE_NAME}-${TERRAFORM_MODULE_SYSTEM}-${TERRAFORM_MODULE_VERSION}.tgz" -C "${TERRAFORM_MODULE_DIR}" --exclude='*/\.*' .
    # Upload
    - 'curl --fail-with-body --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file "${TERRAFORM_MODULE_NAME}-${TERRAFORM_MODULE_SYSTEM}-${TERRAFORM_MODULE_VERSION}.tgz" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/terraform/modules/${TERRAFORM_MODULE_NAME}/${TERRAFORM_MODULE_SYSTEM}/${TERRAFORM_MODULE_VERSION}/file"'
  rules:
    # Tags
    - if: $CI_COMMIT_TAG

pages:
  stage: release
  image: python
  script:
    - pip install mkdocs
    - mkdir public
    - mkdocs build -f docs/mkdocs.yml -d ../public
  artifacts:
    paths:
    - public
  only:
    refs:
      - main
