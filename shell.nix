# shell.nix
{ 
  mkShell,
  helm,
  kind,
  coreutils,
  figlet,
  terraform,
  terraform-docs,
  gnumake,
  jsonnet-tool,
  go-jsonnet,
  jsonnet-bundler,
}:
let

in mkShell {
  name = "gl-infra/terraform-modules/observability/observability-cell-stack";
  packages = [
    helm
    coreutils
    kind
    figlet
    terraform
    terraform-docs
    gnumake
    jsonnet-tool
    go-jsonnet
    jsonnet-bundler
  ];
  shellHook = ''
    source ./scripts/dev-up.sh
    trap \
      "
      ./scripts/dev-down.sh
      " \
      EXIT

    # So we can spawn multiple shells.
    unset shellHook;
  ''; 
} 
