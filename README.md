# `tenant-observability-stack` - Observability stack for single tenant GitLab environments

See documentation in `./docs` and rendered to GitLab Pages.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | 1.6.6 |
| <a name="requirement_helm"></a> [helm](#requirement\_helm) | >= 2.6 |
| <a name="requirement_kubernetes"></a> [kubernetes](#requirement\_kubernetes) | >= 2.5 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_helm"></a> [helm](#provider\_helm) | >= 2.6 |
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | >= 2.5 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [helm_release.cert_exporter](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [helm_release.kube_prometheus_stack](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [helm_release.redis_exporter](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [kubernetes_config_map.cronjob_script](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/config_map) | resource |
| [kubernetes_config_map.dashboards](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/config_map) | resource |
| [kubernetes_cron_job_v1.prom_rules](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/cron_job_v1) | resource |
| [kubernetes_persistent_volume_claim.cronjob_runbooks](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/persistent_volume_claim) | resource |
| [kubernetes_role_binding_v1.prom_rules_generator](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/role_binding_v1) | resource |
| [kubernetes_role_v1.prom_rules_generator](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/role_v1) | resource |
| [kubernetes_secret.redis_password](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/secret) | resource |
| [kubernetes_service_account_v1.prom_rules_generator](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/service_account_v1) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_grafana_password"></a> [grafana\_password](#input\_grafana\_password) | Password for Grafana admin account | `string` | n/a | yes |
| <a name="input_kube_prometheus_additional_values"></a> [kube\_prometheus\_additional\_values](#input\_kube\_prometheus\_additional\_values) | Additional set values to pass to the kube-prometheus-stack helm chart | <pre>list(object({<br>    name  = string<br>    value = string<br>  }))</pre> | n/a | yes |
| <a name="input_kube_prometheus_stack_version"></a> [kube\_prometheus\_stack\_version](#input\_kube\_prometheus\_stack\_version) | Version (without v-prefix) of kube-prometheus-stack helm chart to deploy | `string` | n/a | yes |
| <a name="input_load_dashboards"></a> [load\_dashboards](#input\_load\_dashboards) | A set of dashboards to load for the tenant | <pre>list(object({<br>    path          = string<br>    files         = set(string)<br>    fail_on_empty = optional(bool, true)<br>  }))</pre> | n/a | yes |
| <a name="input_pagerduty_service_key"></a> [pagerduty\_service\_key](#input\_pagerduty\_service\_key) | Pagerduty service key environment secret for alerting | `string` | n/a | yes |
| <a name="input_redis_address"></a> [redis\_address](#input\_redis\_address) | Address of redis instance | `string` | n/a | yes |
| <a name="input_redis_password"></a> [redis\_password](#input\_redis\_password) | Password for tenant redis instance | `string` | n/a | yes |
| <a name="input_slack_url"></a> [slack\_url](#input\_slack\_url) | Slack URL environment secret for alerting | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_monitoring_namespace"></a> [monitoring\_namespace](#output\_monitoring\_namespace) | namespace for the tenant monitoring stack |
| <a name="output_prometheus_service_url"></a> [prometheus\_service\_url](#output\_prometheus\_service\_url) | Service URL for the Prometheus service |
<!-- END_TF_DOCS -->
