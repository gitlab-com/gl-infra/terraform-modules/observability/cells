# Local Development

We use `kind` to boot a local cluster we can deploy this module to.

See below for instructions for both `mise` and `NixOS`.

After deploying the cluster, Prometheus can be accessed through port-forwarding:

```
kubectl port-forward svc/prometheus-operated 9090:9090 -n monitoring
curl http://localhost:9090
```

## Mise-based dependency management

```bash
./scripts/prepare-dev-env.sh

# Start dev environment
source ./scripts/dev-up.sh

# Hack hack hack
terraform plan -var-file=./local.tfvars
terraform apply -var-file=./local.tfvars

# Tear down dev environment
./scripts/dev-down.sh
```

## NixOS

```bash
# Install deps, boot dev environment
nix develop

# Hack hack hack
terraform plan -var-file=./local.tfvars
terraform apply -var-file=./local.tfvars

# Exit shell to trigger teardown behaviour
exit
```
