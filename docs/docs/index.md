# Overview

## What is the `tenant-observability-stack`?

This terraform module provides the common observability tools and services for a single tenant GitLab environment.

This module currently aims to support GCP and AWS. Please do not add cloud-specific resources into this module.

Please attempt to add all non-cloud specific observability to this module, so that it can be shared between AWS
and GCP tenants.

## Updating documentation

We use `mkdocs` to generate this documentation.

In order to render this documentation locally, install `mkdocs` and let it serve content to http://127.0.0.1:8000.

```
pip install mkdocs
cd ./docs
mkdocs serve
```
