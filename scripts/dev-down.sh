#!/usr/bin/env bash

set -euo pipefail

SCRIPT_DIR=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)
MODULE_ROOT="$(cd "$SCRIPT_DIR"/.. && pwd)"
NIX_SHELL_DIR="$MODULE_ROOT/.nix-shell"

# If the .nix-shell directory already exists then just continue as normal, assume
# that this is a second shell and the kind cluster is already provisioned
if [ ! -d "$NIX_SHELL_DIR" ]; then
  echo "$NIX_SHELL_DIR not present, exiting."
  exit 0
fi

# shellcheck disable=SC1091
source "$NIX_SHELL_DIR/env"

# Stop kind.
kind delete cluster --name "$KIND_CLUSTER_NAME"

# Remove all the nonsense.
cd "$MODULE_ROOT"
rm -r "$NIX_SHELL_DIR"
rm -r .terraform/
rm -f terraform.tfstate terraform.tfstate.backup
rm -f .terraform.lock.hcl
